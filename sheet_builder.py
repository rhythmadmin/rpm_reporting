"""Builds an Excel spreadsheet containing the patient data.

    Uses OpenPyXL to generate the spreadsheet.
    Headers are generated dynamically based on the data.
    The data is parsed into a list of lists, which is then converted through loops to rows within the spreadsheet.
    The spreadsheet is then saved to the file system, using a supplied name.

    Usage:
        sheet_builder.create_workbook([data], "filename.xlsx")
"""

import json
from datetime import datetime

import pandas
from openpyxl import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from pytz import timezone
from sanic.log import logger

import config
from db import get_patients, latest_patient_record
from format import format_worksheet


def patient_list_spreadsheet(patients: list) -> None:
    """Creates a spreadsheet using supplied patient data.

    We use pandas here to create a dataframe to store the patient data in
    and then convert the dataframe into a spreadsheet, ready to download.

    Parameters
    -----------
    patients : list
        The list of patients, each a dict, to generate spreadsheet from.
    """

    df = pandas.json_normalize(patients)
    writer = pandas.ExcelWriter('RPM Patient List.xlsx')
    df.to_excel(writer, index=False, sheet_name='Sheet1')
    (max_row, max_col) = df.shape

    for column in df:
        column_length = max(df[column].astype(str).map(len).max(), len(column))
        col_idx = df.columns.get_loc(column)
        writer.sheets['Sheet1'].set_column(col_idx, col_idx, column_length)
        writer.sheets['Sheet1'].autofilter(0, 0, max_row, max_col - 1)

    writer.save()


def convert_sheet_to_json(filename: str, sheet_name: str) -> dict:
    """Converts a spreadsheet to JSON for data ingestion.

    Using a filename and sheet_name, we use pandas here to convert
    the data into a json dict.

    Parameters
    ----------
    filename : str
        The name of the spreadsheet to convert to a json dict.
    sheet_name: str
        The name of the sheet in the file to read data from.

    Returns
    -------
    dict
        A dict containing the converted data.
    """

    excel_dataframe = pandas.read_excel(filename, sheet_name=sheet_name)
    json_str = excel_dataframe.to_json(orient='records')
    dfmt = datetime.now(timezone('UTC')).strftime('%Y-%m-%dT%H:%M:%S.%fZ')
    return {'patients': json.loads(json_str), 'ts': dfmt}


def get_sheet_header(data: dict) -> list:
    """Generates a header row for the spreadsheet using the supplied data.

    By looping through the dates within the data, we get all possible dates to
    add to the header row.

    Parameters
    ----------
    data : dict
        A dictionary containing the data for the spreadsheet.

    Raises
    ------
    NoDataError
        If the supplied data is empty, or the data isn't formatted correctly.

    Returns
    -------
    list
        A list containing the header row.
    """

    try:
        if data:

            header = []

            for key in data.keys():
                for date in data.get(key).keys():
                    if date not in header:
                        header.append(date)

            header.pop(0)
            header.sort()

            header.insert(0, 'Patient')
            header.insert(1, 'Doctor')

            return header
        else:
            raise Exception('NoDataError')
    except Exception as ex:
        logger.error('No data returned from request ' + ex.args[0])


def get_compliance_header() -> list:
    """Generates a simple header for the compliance sheet.

    Returns
    -------
    list
        The header of the compliance sheet.
    """

    return ['Patient', 'Doctor', 'Check-Ins']


def get_daily_report_header(data: dict) -> list:
    """Generates a header for the daily report sheet.

    Dates are formatted by stripping any whitespace and converting them to
    mm.dd.yy notation.

    Parameters
    ----------
    data : dict
        The dictionary containing the patient data.

    Returns
    -------
    list
        The header for the daily report sheet.
    """

    return [
        'Patient',
        'Doctor',
        'Phone Number',
        'Weight',
        'Systolic',
        'Diastolic',
        'Pulse',
        'Glucose',
        'Vendor',
        'Date: {}'.format(
            datetime.strftime(
                datetime.now(),
                '%m.%d.%y @ %H:%M EST'))]


def get_sheet_row_data(
        key: str,
        data: dict,
        query: str,
        patient_info: dict) -> list:
    """Parses the data into a list containing the data for the row in the sheet.

    Parameters
    ----------
    key : str
        The name of the patient to get the data for.
    data : dict
        The data (containing all patients) to be parsed.
    query : str
        The specific type of data to be parsed.
    patient_info : dict
        The dictionary containing the patient metadata.

    Returns
    -------
    list
        A list containing the data for the row in the sheet.
    """

    sheet_row = [key, 'Dr. ' + patient_info.get("Doctor Name").strip()]

    for date in get_sheet_header(data)[2:]:
        if data.get(key) and data.get(key).get(date):
            if query in data.get(key).get(date).keys():
                sheet_row.append(data.get(key).get(date).get(query))
            else:
                sheet_row.append(None)
        else:
            sheet_row.append(None)

    return sheet_row


def get_daily_report_row_data(
        ws: Worksheet,
        data: dict,
        patient_info: dict) -> None:
    """Parses data from multiple fields in the dictionary to return a sheet row.

    Parameters
    ----------
    ws : Worksheet
        The worksheet that will hold the patient data for the daily report.
    data : dict
        The dictionary containing all patient data.
    patient_info : dict
        The dictionary containing the patient metadata.

    Returns
    -------
    list
        A list containing all data for the row in the sheet.
    """

    key = patient_info.get("Patient Name").strip()

    sheet_row = []
    vendor_str = ''

    sheet_row.append(key)
    sheet_row.append('Dr. ' + patient_info.get("Doctor Name").strip())

    if patient_info.get("Phone Number") is not None:
        sheet_row.append(patient_info.get("Phone Number"))
    else:
        sheet_row.append(None)

    for date in get_sheet_header(data)[-1:]:
        if data.get(key) and data.get(key).get(date):

            if config.WEIGHT_KEY_NAME in data.get(key).get(date):
                sheet_row.append(data.get(key).get(
                    date).get(config.WEIGHT_KEY_NAME))
            else:
                sheet_row.append(None)

            if config.SYSTOLIC_KEY_NAME in data.get(key).get(date):
                sheet_row.append(data.get(key).get(
                    date).get(config.SYSTOLIC_KEY_NAME))
            else:
                sheet_row.append(None)

            if config.DIASTOLIC_KEY_NAME in data.get(key).get(date):
                sheet_row.append(data.get(key).get(date).get(
                    config.DIASTOLIC_KEY_NAME))
            else:
                sheet_row.append(None)

            if config.PULSE_KEY_NAME in data.get(key).get(date):
                sheet_row.append(data.get(key).get(
                    date).get(config.PULSE_KEY_NAME))
            else:
                sheet_row.append(None)

            if config.GLUCOSE_KEY_NAME in data.get(key).get(date):
                sheet_row.append(data.get(key).get(
                    date).get(config.GLUCOSE_KEY_NAME))
            else:
                sheet_row.append(None)

            if config.BODYTRACE_KEY_NAME in data.get(key).get(date).keys():
                vendor_str += 'BodyTrace'

            if config.SMART_METER_KEY_NAME or config.iGLUCOSE_KEY_NAME in data.get(key).get(date).keys():
                if len(vendor_str) > 0:
                    vendor_str += ', '
                vendor_str += 'Smart Meter'

    if len(vendor_str) > 0:
        sheet_row.append(vendor_str)
    else:
        sheet_row.append(None)

    if len(sheet_row) < 9:
        while len(sheet_row) < 9:
            sheet_row.append(None)

    sheet_row.append(
        latest_patient_record(
            key, patient_info.get("Doctor Name").strip())[-1])

    ws.append(sheet_row)


def get_compliance_row_data(key: str, data: dict, patient_info: dict) -> list:
    """Counts how many check-ins a patient has had and returns a row with the data.

    Parameters
    ----------
    key : str
        The name of the patient.
    data : dict
        The dictionary containing the patient information.
    patient_info : dict
        The dictionary containing patient metadata.

    Returns
    -------
    list
        A list containing the data for the row in the compliance sheet.
    """

    count = 0
    sheet_row = [key, 'Dr. ' + patient_info.get("Doctor Name").strip()]

    for date in get_sheet_header(data)[2:]:
        if data.get(key) and data.get(key).get(date):
            if len(data.get(key).get(date).keys()) > 3:
                count += 1

    sheet_row.append(count)
    return sheet_row


def create_workbook(data: dict, sheet_name: str) -> None:
    """Creates a .xlsx file using patient data.

    Parameters
    ----------
    data : dict
        A dictionary containing patient data.
    sheet_name : str
        The output file name of the sheet.
    """

    wb = Workbook()
    patients = get_patients()['patients']
    global_sheet_header = get_sheet_header(data)

    weight_ws = wb.active
    weight_ws.title = 'Weight'

    systolic_ws = wb.create_sheet('Systolic')
    diastolic_ws = wb.create_sheet('Diastolic')
    pulse_ws = wb.create_sheet('Pulse')
    glucose_ws = wb.create_sheet('Glucose')

    compliance_ws = wb.create_sheet('Compliance')
    daily_report_ws = wb.create_sheet('Daily Report')

    # Workbook Header
    weight_ws.append(global_sheet_header)
    systolic_ws.append(global_sheet_header)
    diastolic_ws.append(global_sheet_header)
    pulse_ws.append(global_sheet_header)
    glucose_ws.append(global_sheet_header)
    compliance_ws.append(get_compliance_header())
    daily_report_ws.append(get_daily_report_header(data))

    # Append data to each sheet.
    for patient in patients:
        weight_ws.append(
            get_sheet_row_data(
                patient.get("Patient Name").strip(),
                data,
                config.WEIGHT_KEY_NAME,
                patient))
        systolic_ws.append(
            get_sheet_row_data(
                patient.get("Patient Name").strip(),
                data,
                config.SYSTOLIC_KEY_NAME,
                patient))
        diastolic_ws.append(
            get_sheet_row_data(
                patient.get("Patient Name").strip(),
                data,
                config.DIASTOLIC_KEY_NAME,
                patient))
        pulse_ws.append(
            get_sheet_row_data(
                patient.get("Patient Name").strip(),
                data,
                config.PULSE_KEY_NAME,
                patient))
        glucose_ws.append(
            get_sheet_row_data(
                patient.get("Patient Name").strip(),
                data,
                config.GLUCOSE_KEY_NAME,
                patient))
        compliance_ws.append(
            get_compliance_row_data(
                patient.get("Patient Name").strip(),
                data,
                patient))
        get_daily_report_row_data(daily_report_ws, data, patient)

    format_worksheet(wb, daily_report_ws, config.DAILY_REPORT_KEY_NAME, data)
    format_worksheet(wb, weight_ws, config.WEIGHT_KEY_NAME, data)
    format_worksheet(wb, systolic_ws, config.SYSTOLIC_KEY_NAME, data)
    format_worksheet(wb, diastolic_ws, config.DIASTOLIC_KEY_NAME, data)
    format_worksheet(wb, pulse_ws, config.PULSE_KEY_NAME, data)
    format_worksheet(wb, glucose_ws, config.GLUCOSE_KEY_NAME, data)
    format_worksheet(wb, compliance_ws, config.COMPLIANCE_KEY_NAME, data)

    wb.save(sheet_name)
