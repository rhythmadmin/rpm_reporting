"""Generates a PDF based on the patient's data.

    If the patient's data is not available, the PDF will not be generated.
    Some of the queries are not available for all patients.

    The queries are:
        - weight
        - systolic
        - diastolic
        - pulse
        - glucose

    Using matplotlib, the data is plotted and saved as a PNG.
    The PNG is then converted to a PDF and then deleted from disk.
"""
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from fpdf import FPDF
from os import unlink

from sheet_builder import get_sheet_header

import config
import time
import re


def split_series_to_list(series: pd.Series) -> list:
    """Splits a given series into an list containing a list of indices and values.

    Parameters
    ----------
    series : pd.Series
        The series to split.

    Returns
    -------
    list
        The list of indices and values.
    """

    return [series.index, series.values]


def fetch_data_timeline(data: dict, patient: str) -> list:
    """Gets all possible dates for the patient's data.

    Parameters
    ----------
    data : dict
        The dictionary containing all patient data.
    patient : str
        The name of the patient to get dates for.

    Returns
    -------
    list
        A list of dates for the patient's data.
    """

    return [*data.get(patient).keys()]


def plot_patient_data(data: dict, patient: str, query: str) -> bool:
    """Using helper functions and patient data, plot the patient's data according to the query.

    Parameters
    ----------
    data : dict
        The dictionary containing all patient data.
    patient : str
        The name of the patient.
    query : str
        The specific type of plot to generate.

    Returns
    -------
    bool
        Whether the plot is generated successfully.
    """

    patient_data = []
    patient_extra_data = []

    days = pd.to_datetime(get_sheet_header(data)[2:])

    for date in get_sheet_header(data)[2:]:
        if data.get(patient) and data.get(patient).get(date) is not None:
            if query in data.get(patient).get(date):
                patient_data.append(data.get(patient).get(date).get(query))
                if query == config.SYSTOLIC_KEY_NAME:
                    if config.DIASTOLIC_KEY_NAME in data.get(
                            patient).get(date):
                        patient_extra_data.append(data.get(patient).get(
                            date).get(config.DIASTOLIC_KEY_NAME))
            else:
                patient_data.append(np.nan)
                patient_extra_data.append(np.nan)
        else:
            patient_data.append(np.nan)
            patient_extra_data.append(np.nan)

    s1 = pd.Series(patient_data, index=days)

    if not split_series_to_list(s1.dropna())[1].any():
        return False

    plt.plot(*split_series_to_list(s1.dropna()), '-o')
    plt.gcf().autofmt_xdate()
    plt.xlabel('DATE')
    plt.ylabel(query.split('_')[len(query.split('_')) - 1].upper())

    if query == config.SYSTOLIC_KEY_NAME:
        s2 = pd.Series(patient_extra_data, index=days)
        plt.title("SYSTOLIC / DIASTOLIC")
        plt.plot(*split_series_to_list(s2.dropna()), '-o')
        plt.legend(['SYSTOLIC', 'DIASTOLIC'])
    else:
        plt.title(" ".join([*query.split('_')]
                           [:len(query.split('_')) - 1]).upper())

    plt.grid(True, axis='y')

    plt.tight_layout()

    plt.gcf().set_size_inches(10, 4)

    plt.savefig('{}.png'.format(query), format='png')
    plt.close()

    return True


def generate_pdf(
        data: dict,
        doctor: str,
        patient: str,
        month: str,
        notes: str) -> str:
    """Generate a PDF file using patient data.

    Parameters
    ----------
    data : dict
        The dictionary containing the patient data.
    doctor : str
        The name of the patient's doctor.
    patient : str
        The name of the patient.
    month : str
        The month that we are generating the PDF for.
    notes : str, optional
        Any notes that will be included in the PDF.

    Returns
    -------
    str
        The filename of the generated PDF file.
    """

    pdf = FPDF()
    y_counter = 15

    pdf.add_page()
    pdf.set_xy(0, 0)
    pdf.set_font('arial', '', 12)
    pdf.cell(10)
    pdf.cell(70, 10, "Dr. {}".format(doctor), 0, 0, "L")
    pdf.set_font('arial', 'B', 12)
    pdf.cell(60, 10, patient, 0, 0, "C")
    pdf.set_font('arial', '', 12)
    pdf.cell(60,
             10,
             "{} - {}".format(re.sub(r'[\000-\010]|[\013-\014]|[\016-\037]',
                                     '',
                                     time.strftime("%m.%d.%y",
                                                   time.strptime(get_sheet_header(data)[2],
                                                                 "%Y-%m-%d"))),
                              re.sub(r'[\000-\010]|[\013-\014]|[\016-\037]',
                                     '',
                                     time.strftime("%m.%d.%y",
                                                   time.strptime(
                                                       get_sheet_header(
                                                           data)[len(get_sheet_header(data)) - 1],
                                                       "%Y-%m-%d")))),
             0,
             0,
             "R")
    pdf.cell(-200)

    pdf_name = "{} | {} | {}.pdf".format(doctor, patient, month)

    if plot_patient_data(data, patient, config.SYSTOLIC_KEY_NAME):
        pdf.image('{}.png'.format(config.SYSTOLIC_KEY_NAME),
                  x=0, y=y_counter, h=80)
        y_counter += 90
        unlink('{}.png'.format(config.SYSTOLIC_KEY_NAME))

    if plot_patient_data(data, patient, config.WEIGHT_KEY_NAME):
        pdf.image('{}.png'.format(config.WEIGHT_KEY_NAME),
                  x=0, y=y_counter, h=80)
        y_counter += 90
        unlink('{}.png'.format(config.WEIGHT_KEY_NAME))

    if plot_patient_data(data, patient, config.PULSE_KEY_NAME):
        pdf.image('{}.png'.format(config.PULSE_KEY_NAME),
                  x=0, y=y_counter, h=80)
        y_counter += 90
        unlink('{}.png'.format(config.PULSE_KEY_NAME))

    if plot_patient_data(data, patient, config.GLUCOSE_KEY_NAME):
        pdf.add_page()
        y_counter = 10
        pdf.image('{}.png'.format(config.GLUCOSE_KEY_NAME),
                  x=0, y=y_counter, h=80)
        y_counter += 90
        unlink('{}.png'.format(config.GLUCOSE_KEY_NAME))

    if notes:
        pdf.set_xy(0, y_counter)
        pdf.cell(5)
        pdf.multi_cell(200, 7, notes)
        y_counter -= 180

    pdf.output(pdf_name, 'F')

    return pdf_name
