#!/bin/sh

autopep8 --in-place --aggressive --aggressive *.py
flake8 --ignore E501,W504 *.py | grep -v main.py || true