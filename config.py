"""Global constants and configuration variables for the project.

    Some variables are environment variables, which are set locally.
"""

from os import getenv

# Global Constants
DOCTOR_KEY_NAME = 'doctor'
WEIGHT_KEY_NAME = 'weight_lbs'
SYSTOLIC_KEY_NAME = 'systolic_mmhg'
DIASTOLIC_KEY_NAME = 'diastolic_mmhg'
PULSE_KEY_NAME = 'pulse_bpm'
GLUCOSE_KEY_NAME = 'blood_glucose_mgdl'
COMPLIANCE_KEY_NAME = 'compliance'
PATIENT_KEY_NAME = 'name'
DAILY_REPORT_KEY_NAME = 'daily_report'
VENDOR_KEY_NAME = 'vendor'
BODYTRACE_KEY_NAME = 'BodyTrace'
iGLUCOSE_KEY_NAME = 'iGlucose'
SMART_METER_KEY_NAME = 'Smart Meter'
TRANSMISSION_KEY_NAME = 'transmission'

# ElasticSearch
ES_URL = 'https://3.236.225.16:9200/'
ES_HEADERS = {'Content-Type': 'application/json'}
ES_USER = 'admin'
ES_PASS = getenv('ES_PASS')
RPM_INDEX = 'rpm'
RPM_PATIENTS_INDEX = 'rpm_patients'
RPM_ERRORS = 'rpm_errors'

# BodyTrace
BT_URL = 'https://us.data.bodytrace.com/1/device/'
BT_USER = 'rpm32@myrhythmnow.com'
BT_PASS = getenv('BT_PASS')

# iGlucose / Smart Meter
IG_URL = 'https://api.iglucose.com/readings/'
IG_KEY = getenv('IG_KEY')
