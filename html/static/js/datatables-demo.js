// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable({
    "pageLength": 100,
    "lengthMenu": [ 100, 200, 300, 400 ]
  });
});
