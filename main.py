"""The main module is used to test out different functions for debugging purposes.

    A description of each important module is given below:

    db.py
        - Pulls data from ElasticSearch in the function es_query().
        - Pulls specific information from ElasticSearch using different helper functions.
    sheet_builder.py
        - Creates a new spreadsheet using the patient data obtained using the es_query() function in db.py.
            - Builds headers in specified functions within the file.
        - Calls functions in format.py to format each created spreadsheet.
    pdf_builder.py
        - Creates a new pdf using the patient data obtained using the es_query() function in db.py.
        - Uses matplotlib to create new pdf using the patient data obtained using the es_query() function.
        - PDFs are generated using the FPDF library.
    dashboard_api.py
        - Web interface used for many different tasks throughout the RPM Dashboard project.
            - Modification of patient data including the device IDs, phone numbers, and doctor range values.
            - Generation of spreadsheets by month. Each month is a button on the main dashboard page.
            - Generation of patient PDF data using the doctor name, patient name, and a month to query data for.
            - Lists errors encountered by the libraries that fetch vendor data.
    integration.py
        - Grabs data from the vendor sites and creates a new document within the ElasticSearch database.
            - iGlucose (Smart Meter)
            - BodyTrace
        - Converts timestamps to strings.
    format.py
        - Formats rows and columns within the Excel spreadsheet in different ways.
            - Colors alternating rows and columns between the colors gray and white.
            - Widens individual columns depending on the longest value of a cell within the column.
            - Adds alert highlighting to each individual cell, starting with different columns.
            - Sorts patients by names A to Z and doctors from names Z to A.
    config.py
        - Contains information that is used for login/authentication.
            - ElasticSearch credentials.
            - iGlucose/Smart Meter credentials.
        - Contains global constants/values that are used throughout the RPM Dashboard.
"""

from db import es_query, october, load_spreadsheet, latest_patient_record, update_fetch_time, schedule_data_pull, get_patients
from sheet_builder import create_workbook, convert_sheet_to_json

from pdf_builder import plot_patient_data, generate_pdf

from errors import get_errors

import config
import json

# This contains many different placeholder statements that can be used to test out different functions, such as:
# - Plotting patient data from a given dictionary.
# - Generating PDFs using the plotted data.

print(json.dumps(es_query(october), indent=4, sort_keys=True))
# plot_patient_data(es_query(july), 'Ronald Price', config.SYSTOLIC_KEY_NAME)
# generate_pdf(es_query(september), 'Zinsmeister',
#             'Zelick Waganheim', "september", "")
# print(json.dumps(convert_sheet_to_json("RPM_Master.xlsx"), indent=4))
# print(load_spreadsheet('RPM_Master.xlsx', 'Sheet1'))
# print(latest_patient_record("Lenora Maddox", "Okoji"))
# schedule_data_pull(resync=True)
# create_workbook(es_query(october), 'RPM Summary October 2021.xlsx')
# print(json.dumps(get_patients()['doctor_alerts'], indent=4, sort_keys=True))
