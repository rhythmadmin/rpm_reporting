#!/bin/bash

if [[ $ES_PASS == "" || $BT_PASS == "" || $IG_KEY == "" ]]; then
    echo "One of the env vars needed are not set"
    echo "Please read the README.md file"
    echo "ES_PASS=$ES_PASS BT_PASS=$BT_PASS IG_KEY=$IG_KEY"
    exit 1
fi

rm -f docker/localhost.pem
rm -f docker/localhost.key

openssl req -x509 -newkey rsa:4096 -nodes -keyout docker/localhost.key -out docker/localhost.pem -days 365 -sha256 -subj '/CN=localhost'

docker build --network=host -t rpm_nginx -f docker/Dockerfile.nginx .

docker build --network=host -t rpm_service -f docker/Dockerfile .

docker rm -f rpm_service

docker rm -f rpm_nginx

docker run -d -e ES_PASS -e BT_PASS -e IG_KEY --net=host --restart=always --name=rpm_service rpm_service

docker run -d --net=host --restart=always --name=rpm_nginx rpm_nginx

docker system prune -af