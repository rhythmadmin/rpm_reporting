"""Formats patient data within the spreadsheet in different ways.

    Gets doctor-defined alerts for highlighting patient data within a certain threshold.
    Formats the header of the spreadsheet to better suit legibility.
    Formats the rows of the spreadsheet to alternate colors.
    Adjusts the width of each column in the worksheet according to data length.
    Freezes all rows and columns preceding the given cell.

    By using helper functions, the format_worksheet() function can be used to format the header, rows, and columns of the supplied spreadsheet.

    Usage:
        format_worksheet([workbook], [worksheet], [alert query], [data])

    An example of function usage can be found at the bottom of the sheet_builder.py file.
"""

import re
import time
import datetime

from openpyxl import Workbook
from openpyxl.styles import Alignment
from openpyxl.styles import Font
from openpyxl.styles.fills import PatternFill
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.utils.dataframe import dataframe_to_rows

from pandas.core.frame import DataFrame

from db import get_patients

import config


def alert_threshold(
        stat: dict,
        pmax: str,
        pmin: str,
        sysmax: str,
        sysmin: str,
        diamax: str,
        diamin: str,
        glumax: str,
        glumin: str) -> bool:
    """Returns whether the supplied value within the dictionary is within the threshold.

    Parameters
    ----------
    stat : dict
        A dictionary containing the statistic and value of the patient in a key-value pair.
    pmax : str
        A string containing the threshold value for the maximum pulse in the range.
    pmin : str
        A string containing the threshold value for the minimum pulse in the range.
    sysmax : str
        A string containing the threshold value for the maximum systolic value in the range.
    sysmin : str
        A string containing the threshold value for the minimum systolic value in the range.
    diamax : str
        A string containing the threshold value for the maximum diastolic value in the range.
    diamin : str
            A string containing the threshold value for the minimum diastolic value in the range.
    glumax : str
        A string containing the threshold value for the maximum glucose value in the range.
    glumin : str
        A string containing the threshold value for the minimum glucose value in the range.

    Returns
    -------
    bool
        Whether the supplied value is within the threshold.
    """

    if config.PULSE_KEY_NAME in stat.keys():
        if stat[config.PULSE_KEY_NAME] > float(
                pmax) or stat[config.PULSE_KEY_NAME] < float(pmin):
            return True

    if config.SYSTOLIC_KEY_NAME in stat.keys():
        if stat[config.SYSTOLIC_KEY_NAME] > float(
                sysmax) or stat[config.SYSTOLIC_KEY_NAME] < float(sysmin):
            return True

    if config.DIASTOLIC_KEY_NAME in stat.keys():
        if stat[config.DIASTOLIC_KEY_NAME] > float(
                diamax) or stat[config.DIASTOLIC_KEY_NAME] < float(diamin):
            return True

    if config.GLUCOSE_KEY_NAME in stat.keys():
        if stat[config.GLUCOSE_KEY_NAME] > float(
                glumax) or stat[config.GLUCOSE_KEY_NAME] < float(glumin):
            return True

    return False


def format_sheet_header(ws: Worksheet) -> None:
    """Formats the header of the sheet according to Rhythm's style guidelines.

    Parameters
    ----------
    ws : Worksheet
        The worksheet to format the header for.
    """

    for cell in ws[1]:
        if cell.value:
            cell.font = Font(color='FFFFFF', bold=True)
            cell.fill = PatternFill(fgColor='807F80', fill_type='solid')


def format_sheet_rows(ws: Worksheet) -> None:
    """Formats sheet rows to alternate colors.

    Parameters
    ----------
    ws : Worksheet
        The worksheet to format.
    """

    for row in list(ws.iter_rows())[1:]:
        if int(str(row[0].row)) % 2 == 0:
            for cell in row:
                cell.fill = PatternFill(fgColor='EAEAEB', fill_type='solid')
        else:
            for cell in row:
                cell.fill = PatternFill(fgColor='FFFFFF', fill_type='solid')


def fix_sheet_dates(ws: Worksheet) -> None:
    """Formats dates in the worksheet header to better suit legibility.

    Parameters
    ----------
    ws : Worksheet
        The worksheet to format dates in the header for.
    """

    for cell in ws[1]:
        if isinstance(cell.value, str):
            if cell.value[0].isnumeric():
                cell.value = re.sub(
                    r'[\000-\010]|[\013-\014]|[\016-\037]',
                    '',
                    time.strftime(
                        "%m.%d.%y",
                        time.strptime(
                            cell.value,
                            "%Y-%m-%d")))
                cell.alignment = Alignment(horizontal='center')


def freeze_rows_and_columns(ws: Worksheet) -> None:
    """Freezes all rows and columns preceding the cell.

    Parameters
    ----------
    ws : Worksheet
        The worksheet to freeze rows and columns for.
    """

    ws.freeze_panes = "C2"


def find_alerts(wb: Workbook, ws: Worksheet, query: str, data: dict) -> None:
    """Using alert_threshold, formats concerning data points accordingly.

    Parameters
    ----------
    wb : Workbook
        The workbook that contains sheets with patient data.
    ws : Worksheet
        The worksheet containing all patient data.
    query : str
        Which data point to format alerts for.
    data : dict
        The patient data to compare dates with.
    """

    doctor_values = get_patients()['doctor_alerts']
    doctor_defined_ranges = {}

    for col in ws.iter_cols():
        for cell in col:

            doctor_name = ws.cell(
                row=cell.row, column=2).value.split('Dr. ')[1] if ws.cell(
                row=cell.row, column=2).value.strip() != 'Doctor' else None

            if doctor_name is not None:
                for doctor_value in doctor_values:
                    if doctor_value.get("Doctor Name") == doctor_name:
                        doctor_defined_ranges = doctor_value
                        break

            if isinstance(cell.value, float) or isinstance(cell.value, int):
                cell.alignment = Alignment(horizontal='center')

                if not isinstance(cell.font.color.rgb, str):
                    cell.font = Font(color='0064B3')

                if alert_threshold({query: cell.value},
                                   doctor_defined_ranges.get("pulse_gt"),
                                   doctor_defined_ranges.get("pulse_lt"),
                                   doctor_defined_ranges.get("systolic_gt"),
                                   doctor_defined_ranges.get("systolic_lt"),
                                   doctor_defined_ranges.get("diastolic_gt"),
                                   doctor_defined_ranges.get("diastolic_lt"),
                                   doctor_defined_ranges.get("glucose_gt"),
                                   doctor_defined_ranges.get("glucose_lt")):
                    cell.font = Font(bold=True, color='FF0000')

                elif query == config.WEIGHT_KEY_NAME and cell.offset(0, 1).value:
                    if abs(
                            cell.offset(
                                0,
                                1).value -
                            cell.value) > float(
                            doctor_defined_ranges.get("weight_1_day_gt")):
                        cell.offset(
                            0, 1).font = Font(
                            bold=True, color='FF0000')

                elif query == config.COMPLIANCE_KEY_NAME:
                    if cell.value < 16:
                        cell.font = Font(bold=True, color='FF0000')

                elif query == config.DAILY_REPORT_KEY_NAME:
                    if cell.column == 4:
                        if wb['Weight'].cell(
                            row=cell.row,
                            column=wb['Weight'].max_column -
                                1).font.color.rgb == 'FF0000':
                            cell.font = Font(bold=True, color='FF0000')

                    if cell.column == 5:
                        if alert_threshold(
                                {config.SYSTOLIC_KEY_NAME: cell.value}, doctor_defined_ranges.get(
                                    "pulse_gt"),
                                doctor_defined_ranges.get(
                                    "pulse_lt"), doctor_defined_ranges.get("systolic_gt"),
                                doctor_defined_ranges.get(
                                    "systolic_lt"), doctor_defined_ranges.get("diastolic_gt"),
                                doctor_defined_ranges.get(
                                    "diastolic_lt"), doctor_defined_ranges.get("glucose_gt"),
                                doctor_defined_ranges.get("glucose_lt")):
                            cell.font = Font(bold=True, color='FF0000')

                    if cell.column == 6:
                        if alert_threshold(
                                {config.DIASTOLIC_KEY_NAME: cell.value}, doctor_defined_ranges.get(
                                    "pulse_gt"),
                                doctor_defined_ranges.get(
                                    "pulse_lt"), doctor_defined_ranges.get("systolic_gt"),
                                doctor_defined_ranges.get(
                                    "systolic_lt"), doctor_defined_ranges.get("diastolic_gt"),
                                doctor_defined_ranges.get(
                                    "diastolic_lt"), doctor_defined_ranges.get("glucose_gt"),
                                doctor_defined_ranges.get("glucose_lt")):
                            cell.font = Font(bold=True, color='FF0000')

                    if cell.column == 7:
                        if alert_threshold(
                                {config.PULSE_KEY_NAME: cell.value}, doctor_defined_ranges.get(
                                    "pulse_gt"),
                                doctor_defined_ranges.get(
                                    "pulse_lt"), doctor_defined_ranges.get("systolic_gt"),
                                doctor_defined_ranges.get(
                                    "systolic_lt"), doctor_defined_ranges.get("diastolic_gt"),
                                doctor_defined_ranges.get(
                                    "diastolic_lt"), doctor_defined_ranges.get("glucose_gt"),
                                doctor_defined_ranges.get("glucose_lt")):
                            cell.font = Font(bold=True, color='FF0000')

                    if cell.column == 8:
                        if alert_threshold(
                                {config.GLUCOSE_KEY_NAME: cell.value}, doctor_defined_ranges.get(
                                    "pulse_gt"),
                                doctor_defined_ranges.get(
                                    "pulse_lt"), doctor_defined_ranges.get("systolic_gt"),
                                doctor_defined_ranges.get(
                                    "systolic_lt"), doctor_defined_ranges.get("diastolic_gt"),
                                doctor_defined_ranges.get(
                                    "diastolic_lt"), doctor_defined_ranges.get("glucose_gt"),
                                doctor_defined_ranges.get("glucose_lt")):
                            cell.font = Font(bold=True, color='FF0000')

            else:
                if query == config.DAILY_REPORT_KEY_NAME:
                    if cell.row > 1 and cell.column == 1:
                        empty_dict = False

                        # Check each patient's record to see if they have valid
                        # transmissions for the last 3 days.
                        if ws[cell.row][ws.max_column -
                                        1].value != 'No Recent Transmissions':
                            if abs((datetime.datetime.strptime(
                                    ws[cell.row][ws.max_column - 1].value,
                                    "%m.%d.%y @ %H:%M:%S EST") - datetime.datetime.now()).total_seconds() / 3600) >= 72:
                                empty_dict = True
                        elif data.get(cell.value) and len(data.get(cell.value).keys()) < 3 and ws[cell.row][
                                ws.max_column - 1].value == "No Recent Transmissions":
                            empty_dict = False
                        elif not data.get(cell.value):
                            empty_dict = True
                        else:
                            empty_dict = True

                        if empty_dict:
                            cell.font = Font(color='E6A62E', bold=True)
                        else:
                            cell.font = Font(color='717071')

                    else:
                        cell.font = Font(color='717071')
                else:
                    if cell.row > 1 and cell.column == 1:
                        if wb["Daily Report"].cell(
                                row=cell.row, column=cell.column).font.color.rgb == '00E6A62E':
                            cell.font = Font(color='00E6A62E', bold=True)
                        else:
                            cell.font = Font(color='717071')
                    else:
                        cell.font = Font(color='717071')


def adjust_worksheet_width(ws: Worksheet) -> None:
    """Adjusts the width of each column in the worksheet according to data length.

    Parameters
    ----------
    ws : Worksheet
        The worksheet to adjust the column width for.
    """

    for col in ws.iter_cols():
        max_length = 0
        column = col[0].column_letter
        for cell in col:
            try:
                if len(str(cell.value)) > max_length:
                    max_length = len(str(cell.value))
            except BaseException:
                pass
        adjusted_width = max_length * 1.2
        ws.column_dimensions[column].width = adjusted_width


def sort_patients(ws: Worksheet) -> None:
    """Using pandas to sort the data, sorts patients by doctor and name.

    Parameters
    ----------
    ws : Worksheet
        The worksheet to sort.
    """

    df = DataFrame(list(ws.values)[1:])
    df.sort_values([1, 0], inplace=True, ascending=[False, True])

    sorted_data = []
    for dfRow in dataframe_to_rows(df, index=False, header=False):
        sorted_data.append(dfRow)

    for x in range(ws.max_row, 0, -1):
        if x != 0 and x != 1:
            ws.delete_rows(x)

    for x in range(len(sorted_data)):
        ws.append(sorted_data[x])


def format_worksheet(
        wb: Workbook,
        ws: Worksheet,
        query: str,
        data: dict) -> None:
    """Formats a worksheet using helper functions defined above.

    Parameters
    ----------
    wb : Workbook
        The workbook containing the sheets to be formatted.
    ws : Worksheet
        The worksheet to be formatted.
    query : str
        The query string to be used to format the worksheet data with.
    data : dict
        The data being used to format the worksheet data with.
    """

    sort_patients(ws)
    adjust_worksheet_width(ws)
    fix_sheet_dates(ws)
    format_sheet_rows(ws)
    find_alerts(wb, ws, query, data)
    format_sheet_header(ws)
    freeze_rows_and_columns(ws)
