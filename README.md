# RPM Service


## Configuration

Environment vars needed to run the service either from your local machine or set in containers runtime.

```sh
# Elasticsearch Password
ES_PASS=""

# BodyTrace Password 
BT_PASS=""

# iGlucose API Key
IG_KEY=""
```

## Run Locally

In your shell env

```sh
export BT_PASS=""
export IG_KEY=""
export ES_PASS=""
```

If you do not run in docker, your machine will need the following deps:

- Python 3.6 or above
- pip3
- `pip3 install -r ./requirements.txt`

After deps are present, run `python3 ./dashboard-api.py`

If you Ctrl+C `dashboard-api.py` it might not exit because of the background jobs running. You'll have to Ctrl+Z (push to background) and then `kill -9` the process id.

You can also change:

`app.run(host='127.0.0.1', port=8080)` to `app.run(host='127.0.0.1', port=8080, debug=True)`

To run in debug mode and it will pick up any local changes to the python files so you don't have to stop and start the `dashboard-api.py` file.

## Docker Build & Run

```sh
./build_run.sh
```

## Routes


## Sites

RPM Report Site: https://rpmreport.rhythmsynergy.com/

RPM Kibana: https://rpm.rhythmsynergy.com/app

Elasticsearch Endpoint: https://3.236.225.16:9200/


## Other Notes

- Basic Auth is enforced by NGINX and the encrypted username and password are set in the `.htpasswd` file.

- ES Index, rpm, holds all vendor device data

- ES Index, rpm_patients, holds all doctor patient information and will also include alert thresholds per doctor in the coming weeks

- RPM Elasticsearch will be migrated to Integration Data Store in the coming weeks

- To access ElasticSearch from your local machine you'll have to add your IPv4 public IP to the security group, sg-fca305d3 (default)
    - All All Your_Public_IP/32