"""Used to grab data from the ES cluster.

    Monthly queries are used to get the latest data for that month from the ES cluster.

    A prototype dynamic monthly query is available to be implemented in the future.
"""

import calendar
import urllib3
import requests
import json
import pytz
import threading
import schedule
import time

from datetime import datetime, timedelta, timezone as tz
from pytz import timezone

import config
from integration import GetDeviceData

urllib3.disable_warnings()

# ES Query Params
rpm_url = config.ES_URL + config.RPM_INDEX + '/_search?'
patients_url = config.ES_URL + config.RPM_PATIENTS_INDEX + '/_doc/1'
user = config.ES_USER
pw = config.ES_PASS
headers = config.ES_HEADERS

# ES Query Month Ranges
selected_year = 2021
selected_month = 12


lt_str = "{}-{}-{}".format(
    selected_year +
    1 if selected_month > 11 else selected_year,
    "0{}".format(
        selected_month -
        11) if selected_month > 11 else selected_month +
    1 if selected_month +
    1 > 9 else "0{}".format(
        selected_month +
        1),
    "01")
gt_str = "{}-{}-{}".format(selected_year, selected_month -
                           1 if selected_month -
                           1 > 9 else "0{}".format(selected_month -
                                                   1), calendar.monthrange(selected_year, selected_month)[1])

june = {"lt": "2021-07-01", "gt": "2021-05-31", "format": "yyyy-MM-dd"}
july = {"lt": "2021-08-01", "gt": "2021-06-30", "format": "yyyy-MM-dd"}
august = {"lt": "2021-09-01", "gt": "2021-07-31", "format": "yyyy-MM-dd"}
september = {"lt": "2021-10-01", "gt": "2021-08-31", "format": "yyyy-MM-dd"}
october = {"lt": "2021-11-01", "gt": "2021-09-30", "format": "yyyy-MM-dd"}
november = {"lt": "2021-12-01", "gt": "2021-10-31", "format": "yyyy-MM-dd"}
december = {"lt": "2022-01-01", "gt": "2021-11-30", "format": "yyyy-MM-dd"}
january = {"lt": "2022-02-01", "gt": "2021-12-31", "format": "yyyy-MM-dd"}
february = {"lt": "2022-03-01", "gt": "2022-01-31", "format": "yyyy-MM-dd"}

month_query = {"lt": lt_str, "gt": gt_str, "format": "yyyy-MM-dd"}


def load_spreadsheet(filename: str, sheet_name: str) -> json:
    """Grabs a spreadsheet, converts it into json and uploads it to ES.

    Parameters
    ----------
    filename : str
        The name of the spreadsheet file to be converted.
    sheet_name : str
        The name of the sheet within the main spreadsheet file.

    Returns
    -------
    json
        The response from the ES server.
    """

    from sheet_builder import convert_sheet_to_json

    patients = convert_sheet_to_json(filename, sheet_name)

    dt = datetime.now(timezone('US/Eastern')
                      ).strftime('%m.%d.%Y @ %H:%M:%S EST')

    data = {'patients': [], 'ts': patients['ts'], 'last_fetch': dt}

    for e in patients['patients']:

        patient = e['Patient Name'].strip()
        doctor = e['Doctor Name'].strip()
        phone = e['Phone Number']
        bt_bp = e['BodyTrace BP DeviceID']
        bt_scale = e['BodyTrace Scale DeviceID']
        sm_bp = e['SM BP DeviceID']
        sm_glucometer = e['SM Glucometer DeviceID']
        sm_scale = e['SM Scale DeviceID']

        if bt_bp:
            bt_bp = str(bt_bp).rstrip('.').strip()
        if bt_scale:
            bt_scale = str(bt_scale).rstrip('.').strip()
        if sm_bp:
            sm_bp = str(sm_bp).rstrip('.').strip()
        if sm_glucometer:
            sm_glucometer = str(sm_glucometer).rstrip('.').strip()
        if sm_scale:
            sm_scale = str(sm_scale).rstrip('.').strip()

        patient = {
            'Patient Name': patient,
            'Doctor Name': doctor,
            'Phone Number': phone,
            'BodyTrace BP DeviceID': bt_bp,
            'BodyTrace Scale DeviceID': bt_scale,
            'SM BP DeviceID': sm_bp,
            'SM Glucometer DeviceID': sm_glucometer,
            'SM Scale DeviceID': sm_scale}

        data['patients'].append(patient)

    data = json.dumps(data).replace('" "', 'null')

    resp = requests.post(
        url=patients_url,
        verify=False,
        data=data,
        auth=(
            user,
            pw),
        headers=headers)

    schedule_data_pull()

    return resp.json()


def get_patients() -> dict:
    """Gets the current list of patients in the ES cluster.

    Returns
    -------
    dict
        Patient list
    """

    resp = requests.get(
        url=patients_url,
        verify=False,
        auth=(
            user,
            pw),
        headers=headers)

    return resp.json()['_source']


def schedule_data_pull(resync=False) -> None:
    """Main data pull/fetch method used in the schedule library"""

    hdt = datetime.utcnow().replace(
        minute=0, second=0, microsecond=0, tzinfo=tz.utc)
    to_t = hdt.timestamp() * 1000.0
    hdt2 = hdt + timedelta(hours=-1)
    from_t = hdt2.timestamp() * 1000.0

    if resync:
        time_parse = "%Y-%m-%d_%H:%M %Z"
        from_t = datetime.strptime(
            "2021-06-01_00:00 UTC",
            time_parse).timestamp() * 1000.0

    GetDeviceData().fetch_all(from_t, to_t)
    update_fetch_time()


def run_jobs(interval=10) -> threading.Event:
    """Run schedule jobs in the background

    Returns
    -------
    threading.Event
        The event that will run to schedule jobs.
    """

    cease_continuous_run = threading.Event()

    class ScheduleThread(threading.Thread):
        @classmethod
        def run(cls):
            while not cease_continuous_run.is_set():
                schedule.run_pending()
                time.sleep(interval)

    continuous_thread = ScheduleThread()
    continuous_thread.start()
    return cease_continuous_run


def update_fetch_time() -> None:
    """Updates the last time a data sync happened in the ES cluster."""

    resp = requests.get(
        url=patients_url,
        verify=False,
        auth=(
            user,
            pw),
        headers=headers)

    data = resp.json()['_source']
    dt = datetime.now(timezone('US/Eastern')
                      ).strftime('%m.%d.%Y @ %H:%M:%S EST')
    data['last_fetch'] = dt

    requests.post(
        url=patients_url,
        verify=False,
        data=json.dumps(data),
        auth=(
            user,
            pw),
        headers=headers)


def get_latest_fetch() -> str:
    """Gets the last time a data sync happened in the ES cluster.

    Returns
    -------
    string
        The latest fetch time from patient document.
    """

    resp = requests.get(
        url=patients_url,
        verify=False,
        auth=(
            user,
            pw),
        headers=headers)

    if 'last_fetch' not in resp.json()['_source']:
        return 'error getting latest data pull'

    return resp.json()['_source']['last_fetch']


def remove_patient(patient: str, doctor: str) -> requests.Response:
    """Removes a patient using the supplied name and doctor.

    Parameters
    ----------
    patient : str
        The name of the patient to remove.
    doctor : str
        The name of the patient's doctor.

    Returns
    -------
    requests.Response
        The response from the ES server.
    """

    patients = get_patients()

    for e in patients['patients']:
        if e['Patient Name'] == patient and e['Doctor Name'] == doctor:
            patients['patients'].remove(e)

    dfmt = datetime.now(timezone('UTC')).strftime('%Y-%m-%dT%H:%M:%S.%fZ')

    patients['ts'] = dfmt
    data = json.dumps(patients)

    resp = requests.post(
        url=patients_url,
        verify=False,
        data=data,
        auth=(
            user,
            pw),
        headers=headers)

    return resp


def add_patient(data: dict) -> requests.Response:
    """Adds a new patient to the ES cluster and returns the response.

    Parameters
    ----------
    data : dict
        The new patient's data to add.

    Returns
    -------
    resp : requests.Response
        The response from the ES server.
    """

    patients = get_patients()

    for e in patients['patients']:
        if e['Patient Name'] == data['Patient Name'] and e['Doctor Name'] == data['Doctor Name']:
            patients['patients'].remove(e)

    patients['patients'].append(data)

    dfmt = datetime.now(timezone('UTC')).strftime('%Y-%m-%dT%H:%M:%S.%fZ')

    patients['ts'] = dfmt

    data = json.dumps(patients)

    resp = requests.post(
        url=patients_url,
        verify=False,
        data=data,
        auth=(
            user,
            pw),
        headers=headers)

    return resp


def update_doctor(data: dict) -> requests.Response:
    """Updates a doctor in the ES cluster and returns the response.

    Parameters
    ----------
    data : dict
        The doctor's updated data.

    Returns
    -------
    resp : requests.Response
        The response from the ES server.
    """

    patients = get_patients()

    for doctor in patients['doctor_alerts']:
        if doctor["Doctor Name"] == data["Doctor Name"]:
            patients['doctor_alerts'].remove(doctor)

    patients['doctor_alerts'].append(data)

    dfmt = datetime.now(timezone('UTC')).strftime('%Y-%m-%dT%H:%M:%S.%fZ')

    patients['ts'] = dfmt

    data = json.dumps(patients)

    resp = requests.post(
        url=patients_url,
        verify=False,
        data=data,
        auth=(
            user,
            pw),
        headers=headers)

    return resp


def latest_sync() -> str:
    """Gets the most recent record from the ES cluster.

    Returns
    -------
    str
        The most recent record's timestamp.
    """

    query = {
        "size": 1,
        "sort": {"ts": "desc"},
        "query": {
            "match_all": {}
        }
    }

    resp = requests.post(
        url=rpm_url,
        verify=False,
        data=json.dumps(query),
        auth=(
            user,
            pw),
        headers=headers)

    time = resp.json()['hits']['hits'][0]['_source']['ts']
    if time:
        utc = pytz.timezone('UTC')
        est = pytz.timezone('US/Eastern')

        utcTimestamp: datetime = None

        try:
            utcTimestamp = datetime.strptime(
                time, '%Y-%m-%dT%H:%M:%S.%f')
            utcTimestamp = utc.localize(utcTimestamp)
        except ValueError:
            utcTimestamp = datetime.strptime(
                time + '.000', '%Y-%m-%dT%H:%M:%S.%f')
            utcTimestamp = utc.localize(utcTimestamp)

        return utcTimestamp.astimezone(
            est).strftime('%m.%d.%Y @ %H:%M:%S EST')

    return 'No Recent Events'


def latest_patient_record(patient: str, doctor: str) -> list:
    """Gets the most recent record for a patient from the ES cluster.

    Parameters
    ----------
    patient : str
        The patient name to search record for.
    doctor : str
        The patient's doctor to filter by.

    Returns
    -------
    list
        The most recent patient record.
    """

    query = {
        "size": 50,
        "sort": {
            "ts": "desc"
        },
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "name": patient
                        }
                    },
                    {
                        "match": {
                            "doctor": doctor
                        }
                    }
                ]
            }
        }
    }

    resp = requests.post(
        url=rpm_url,
        verify=False,
        data=json.dumps(query),
        auth=(
            user,
            pw),
        headers=headers)

    latest_record = None
    returned_data = None

    for hit in resp.json()['hits']['hits']:
        if hit['_source'].get('name').strip() == patient.strip():
            returned_data = hit
            latest_record = hit['_source']['ts']
            break

    if latest_record:
        utc = pytz.timezone('UTC')
        est = pytz.timezone('US/Eastern')

        utcTimestamp: datetime = None

        try:
            utcTimestamp = datetime.strptime(
                latest_record, '%Y-%m-%dT%H:%M:%S.%f')
            utcTimestamp = utc.localize(utcTimestamp)
        except ValueError:
            utcTimestamp = datetime.strptime(
                latest_record + '.000', '%Y-%m-%dT%H:%M:%S.%f')
            utcTimestamp = utc.localize(utcTimestamp)

        return [returned_data, utcTimestamp.astimezone(
            est).strftime('%m.%d.%y @ %H:%M:%S EST')]

    return ['No Recent Transmissions']


def es_query(month: dict) -> dict:
    """Queries the ES cluster for transmissions in a given month.

    Parameters
    ----------
    month : dict
        The dictionary containing the month information for which to query.

    Returns
    -------
    dict
        The data of all patients and their transmissions for the given month.
    """

    query = {
        "query": {
            "bool": {
                "filter": [
                    {
                        "range": {
                            "ts": {}}}]}},
        "size": 100000}

    query['query']['bool']['filter'][0]['range']['ts'] = month

    resp = requests.post(
        url=rpm_url,
        verify=False,
        data=json.dumps(query),
        auth=(
            user,
            pw),
        headers=headers)

    data = {}

    for i in sorted(
            resp.json()['hits']['hits'],
            key=lambda k: k['_source']['ts']):

        if 'name' in i['_source'].keys() and len(i['_source']['name']) > 0:
            name = i['_source']['name'].strip()

            if name not in data.keys():
                data[name] = {}

        if 'ts' in i['_source'].keys():

            utc = pytz.timezone('UTC')
            est = pytz.timezone('US/Eastern')

            utcTimestamp: datetime = None

            try:
                utcTimestamp = datetime.strptime(
                    i['_source']['ts'], '%Y-%m-%dT%H:%M:%S.%f')
                utcTimestamp = utc.localize(utcTimestamp)
            except ValueError:
                utcTimestamp = datetime.strptime(
                    i['_source']['ts'] + '.000', '%Y-%m-%dT%H:%M:%S.%f')
                utcTimestamp = utc.localize(utcTimestamp)

            convertedTime = utcTimestamp.astimezone(
                est).strftime('%Y-%m-%dT%H:%M:%S.%f')

            ts = convertedTime.split('T', 1)[0]

            if ts not in data[name].keys():
                data[name][ts] = {}

        if config.DOCTOR_KEY_NAME in i['_source'].keys():
            data[name][ts][config.DOCTOR_KEY_NAME] = i['_source'][config.DOCTOR_KEY_NAME]

        if config.PULSE_KEY_NAME in i['_source'].keys():
            data[name][ts][config.PULSE_KEY_NAME] = i['_source'][config.PULSE_KEY_NAME]

        if config.SYSTOLIC_KEY_NAME in i['_source'].keys():
            data[name][ts][config.SYSTOLIC_KEY_NAME] = i['_source'][config.SYSTOLIC_KEY_NAME]

        if config.DIASTOLIC_KEY_NAME in i['_source'].keys():
            data[name][ts][config.DIASTOLIC_KEY_NAME] = i['_source'][config.DIASTOLIC_KEY_NAME]

        if config.WEIGHT_KEY_NAME in i['_source'].keys():
            data[name][ts][config.WEIGHT_KEY_NAME] = i['_source'][config.WEIGHT_KEY_NAME]

        if config.GLUCOSE_KEY_NAME in i['_source'].keys():
            if config.GLUCOSE_KEY_NAME not in data[name][ts]:
                data[name][ts][config.GLUCOSE_KEY_NAME] = i['_source'][config.GLUCOSE_KEY_NAME]

        if config.VENDOR_KEY_NAME in i['_source'].keys():
            if i['_source'][config.VENDOR_KEY_NAME] not in data[name][ts]:
                data[name][ts][i['_source'][config.VENDOR_KEY_NAME]] = True

        if config.TRANSMISSION_KEY_NAME not in data[name][ts]:
            data[name][ts][config.TRANSMISSION_KEY_NAME] = convertedTime

    return data


def pdf_query() -> dict:
    """Queries the ES cluster for patient and doctor names.

    Returns
    -------
    dict
        A dictionary with patient and doctor names.
    """

    query = {
        "query": {
            "bool": {
                "must": [
                    {
                        "exists": {
                            "field": "doctor"
                        }
                    },
                    {
                        "exists": {
                            "field": "name"
                        }
                    }
                ]
            }
        },
        "size": 100000
    }

    resp = requests.post(
        url=rpm_url,
        verify=False,
        data=json.dumps(query),
        auth=(
            user,
            pw),
        headers=headers)

    data = {}
    months = [
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
        "January",
        "February"]

    for i in resp.json()['hits']['hits']:

        doctor = i['_source'][config.DOCTOR_KEY_NAME].strip()
        name = i['_source'][config.PATIENT_KEY_NAME].strip()

        if doctor != "" or name != "":

            if doctor not in data.keys():
                data[doctor] = {}

            if name not in data[doctor].keys():
                data[doctor][name] = months

    return data
