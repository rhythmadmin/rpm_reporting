"""Sets a default value for newly onboarded doctors.

    If a doctor does not have any alert values assigned to them, the values within the doctor_metrics object are assigned to the doctor.
"""

from db import get_patients
import json

patients = get_patients()

doctor_alerts = []

for e in patients['patients']:
    found = False
    for a in patients['doctor_alerts']:
        if a['Doctor Name'] == e['Doctor Name']:
            found = True

    if not found:
        doctor_metrics = {
            "Doctor Name": e['Doctor Name'],
            "pulse_gt": 110,
            "pulse_lt": 50,
            "systolic_gt": 180,
            "systolic_lt": 90,
            "diastolic_gt": 110,
            "diastolic_lt": 50,
            "glucose_gt": 250,
            "glucose_lt": 70,
            "weight_1_day_gt": 3,
            "weight_7_day_gt": 5
        }

        doctor_alerts.append(doctor_metrics)

doctor_alerts = [i for n, i in enumerate(
    doctor_alerts) if i not in doctor_alerts[n + 1:]]

patients["doctor_alerts"] + doctor_alerts

data = json.dumps(patients)

print(data)
