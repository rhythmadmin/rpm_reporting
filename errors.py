"""Catches and logs errors to the RPM dashboard.

    This logging function can be accessed through the 'errors' tab within the RPM Dashboard site.
"""

import requests
from datetime import datetime
from pytz import timezone
import config
import json

error_url = config.ES_URL + config.RPM_ERRORS
user = config.ES_USER
pw = config.ES_PASS
headers = config.ES_HEADERS


def send_error(err: str):
    est = timezone('EST')
    dtnow = datetime.now(est).strftime('%Y-%m-%dT%H:%M:%S.%fZ')
    data = {'ts': dtnow, 'error': str(err)}
    requests.post(
        url=error_url + '/_doc/',
        verify=False,
        data=json.dumps(data),
        auth=(
            user,
            pw),
        headers=headers)
    print(data)


def get_errors() -> list:
    """Grabs all errors stored within the ElasticSearch cluster.

    Returns
    --------
        list: A list of errors encountered when trying to grab data from vendor sites.
    """
    query = {
        "size": 50,
        "sort": {"ts": "desc"},
        "query": {
            "match_all": {}
        }
    }

    resp = requests.post(
        url=error_url + '/_search?',
        verify=False,
        data=json.dumps(query),
        auth=(
            user,
            pw),
        headers=headers)

    data = []
    for e in resp.json()['hits']['hits']:
        data.append(e['_source'])

    return data
