"""Grabs data from different vendor databases and adds them to the ElasticSearch database.

    Usage:
        resync_data.py
"""
import requests
import config
import urllib3
import datetime as dt
from json import dumps
from integration import GetDeviceData

urllib3.disable_warnings()

url = config.ES_URL + config.RPM_INDEX
es_user = config.ES_USER
es_pass = config.ES_PASS
headers = config.ES_HEADERS

delete_index = requests.delete(
    url=url,
    verify=False,
    auth=(
        es_user,
        es_pass),
    headers=headers)

create_index = requests.put(
    url=url,
    verify=False,
    auth=(
        es_user,
        es_pass),
    headers=headers)

update_index = requests.put(
    url=url + '/_settings',
    verify=False,
    data=dumps({"index.max_result_window": 100000}),
    auth=(
        es_user,
        es_pass),
    headers=headers)

print("Delete Index " + url + " " + delete_index.text)
print("Create Index " + url + " " + create_index.text)
print("Update Index " + url + " " + update_index.text)

hdt = dt.datetime.utcnow().replace(
    minute=0, second=0, microsecond=0, tzinfo=dt.timezone.utc)

to_t = hdt.timestamp() * 1000.0

time_parse = "%Y-%m-%d_%H:%M %Z"

to_t = dt.datetime.strptime(
    "2021-10-31_23:59 UTC",
    time_parse).timestamp() * 1000.0

from_t = dt.datetime.strptime(
    "2021-10-01_00:00 UTC",
    time_parse).timestamp() * 1000.0

load_count = GetDeviceData().fetch_all(from_t, to_t)

print(
    "Loaded " +
    str(load_count) +
    " measurements from " +
    str(from_t) +
    " to " +
    str(to_t))
