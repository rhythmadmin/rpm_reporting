"""This module is responsible for fetching data from patients' devices.

    The different vendor sites (Smart Meter and BodyTrace) are accessed and the data is pulled into the ElasticSearch database.

    The module also includes functions to convert timestamps to strings.

    Usage:
        python3 integration.py [catchup] [from] [to]
"""
from errors import send_error
import requests
import datetime as dt
import urllib3
import config
from sys import argv
from json import dumps
from sanic.log import logger

urllib3.disable_warnings()


class GetDeviceData:
    """A class for retrieving device data from different vendors."""

    es_user = config.ES_USER
    es_pass = config.ES_PASS
    headers = config.ES_HEADERS
    rpm_url = config.ES_URL + config.RPM_INDEX + '/_doc/'
    patients_url = config.ES_URL + config.RPM_PATIENTS_INDEX + '/_doc/1'

    bt_url = config.BT_URL
    bt_user = config.BT_USER
    bt_pass = config.BT_PASS

    ig_key = config.IG_KEY
    ig_url = config.IG_URL

    T_INTERVAL = 1000.0 * 60.0 * 60.0

    LBS_PER_G = .00220462
    MMHG_PER_PA = .00750062

    def append_devices(
            self,
            patient_name,
            doctor_name,
            vendor,
            devtype,
            cellval) -> None:
        """Appends devices to the patient's device list.

        Parameters
        ----------
        self
            The class instance.
        patient_name
            The name of the patient.
        doctor_name
            The name of the doctor.
        vendor
            The name of the vendor.
        devtype
            The type of device being added to the patient's device list.
        cellval
            The serial number of the device (if it exists).
        """

        if cellval is None:
            return
        cellstr = str(cellval).strip()
        if len(cellstr) < 1:
            return
        if cellstr.endswith(".0"):
            cellstr = cellstr[0:len(cellstr) - 2]
        self.devices.append(
            (patient_name, doctor_name, vendor, devtype, cellstr))

    def __init__(self):
        self.sess = requests.Session()

        self.es = self.sess.get(
            url=GetDeviceData.patients_url,
            verify=False,
            auth=(
                GetDeviceData.es_user,
                GetDeviceData.es_pass),
            headers=GetDeviceData.headers)

        patients = self.es.json()['_source']['patients']
        self.devices = []
        for p in patients:
            patient_name = p["Patient Name"].strip()
            doctor_name = p["Doctor Name"].strip()

            self.append_devices(
                patient_name,
                doctor_name,
                "BodyTrace",
                "bp",
                p["BodyTrace BP DeviceID"] if "BodyTrace BP DeviceID" in p else None)
            self.append_devices(
                patient_name,
                doctor_name,
                "BodyTrace",
                "scale",
                p["BodyTrace Scale DeviceID"] if "BodyTrace Scale DeviceID" in p else None)
            self.append_devices(
                patient_name,
                doctor_name,
                "Smart Meter",
                "bp",
                p["SM BP DeviceID"] if "SM BP DeviceID" in p else None)
            self.append_devices(
                patient_name,
                doctor_name,
                "Smart Meter",
                "glucometer",
                p["SM Glucometer DeviceID"] if "SM Glucometer DeviceID" in p else None)
            self.append_devices(
                patient_name,
                doctor_name,
                "Smart Meter",
                "scale",
                p["SM Scale DeviceID"] if "SM Scale DeviceID" in p else None)

    def convert_ts(unixts) -> str:
        """Converts a unix timestamp to a string.

        Parameters
        ----------
        unixts
            The unix-formatted timestamp to convert.

        Returns
        -------
        str
            The converted timestamp.
        """

        unixt = dt.datetime.fromtimestamp(float(unixts) / 1000.0)
        strtime = unixt.strftime("%Y-%m-%dT%H:%M:%S.%f")
        strtime = strtime[0:len(strtime) - 3]
        return strtime

    def fetch_all(self, from_t, to_t):
        """Fetches all devices that are stored in the self.devices array within a specified time range.

        Parameters
        ----------
        from_t : int
            The start time of the range.
        to_t : int
            The end time of the range.

        Returns
        -------
        int
            The amount of devices that have been fetched.
        """

        load_count = 0
        for dev in self.devices:
            load_count += self.fetch(dev, from_t, to_t)
        return load_count

    def fetch(self, dev, from_t, to_t):
        """Uses helper methods to fetch devices from different vendors.

        Parameters
        ----------
        dev : array
            An array of different strings representing different device vendors.
        from_t : int
            The start time of the range used to fetch devices.
        to_t : int
            The end time of the range.

        Returns
        -------
        int
            Returns the number of devices that were fetched based on the value of the 3rd element of the dev array or 0.
        """

        if dev[2] == "BodyTrace":
            return self.fetch_bt(dev, from_t, to_t)
        elif dev[2] == "Smart Meter":
            return self.fetch_sm(dev, from_t, to_t)
        else:
            err = "unknown vendor: " + dev[2]
            logger.error(err)
            send_error(err)
        return 0

    def ts_str(ts):
        """Converts a timestamp to a string.

        Parameters
        ----------
        ts
            The timestamp to be converted.

        Returns
        -------
        str
            The string of the timestamp.
        """

        dts = dt.datetime.fromtimestamp(ts / 1000.0)
        s = dts.strftime("%Y-%m-%dT%H:%M:%S")
        return s

    def post_doc(self, esdoc, id):
        """Updates the ElasticSearch instance with the new data.

        Parameters
        ----------
        esdoc : dict
            The ElasticSearch document to upload.
        id : str
            A generated id for the document.
        """

        self.sess.post(
            url=GetDeviceData.rpm_url + id + '?op_type=create',
            verify=False,
            data=dumps(esdoc),
            auth=(
                GetDeviceData.es_user,
                GetDeviceData.es_pass),
            headers=GetDeviceData.headers)

    def fetch_sm(self, dev, from_t, to_t):
        """Fetches device data from Smart Meter.

        Parameters
        ----------
        dev : array
            The array that contains information for retrieving device data.
        from_t : int
            The time at which the device data should be retrieved from.
        to_t : int
            The time at which the device data should stop being retrieved.

        Returns
        -------
        int
            The amount of devices that were loaded from Smart Meter.
        """

        url = GetDeviceData.ig_url
        props = {}
        props["api_key"] = GetDeviceData.ig_key
        props["date_start"] = GetDeviceData.ts_str(from_t)
        props["date_end"] = GetDeviceData.ts_str(to_t)
        props["device_ids"] = [dev[4]]

        resp = self.sess.post(url, json=props)
        rj = None
        try:
            rj = resp.json()
        except BaseException as e:
            logger.error(str(e))
            send_error(str(e))
            pass

        load_count = 0

        if rj is not None and len(rj) > 0:
            for esdoc in rj["readings"]:
                esdoc["name"] = dev[0].strip()
                esdoc["doctor"] = dev[1].strip()
                esdoc["vendor"] = dev[2]
                esdoc["deviceType"] = dev[3]
                esdoc["deviceId"] = dev[4]
                dr = esdoc.get("date_recorded")
                if dr is not None:
                    esdoc["ts"] = dr
                bat = esdoc.get("battery")
                if bat is not None:
                    esdoc["battery_pct"] = bat

                id = esdoc["deviceId"] + esdoc["ts"]
                self.post_doc(esdoc, id)
                load_count += 1

        return load_count

    def fetch_bt(self, dev, from_t, to_t):
        """Fetches device information and data for the given device from BodyTrace.

        Parameters
        ----------
        dev : array
            The array containing the device information.
        from_t : int
            The start time used to get the device information from.
        to_t : int
            The end time used to get the device information from.

        Returns
        -------
        int
            The amount of devices that were loaded into esdoc.

        Raises
        ------
        Exception
            If the connection to the ElasticSearch instance fails.
        """

        url = GetDeviceData.bt_url + dev[4] + "/datamessages"
        url += "?from=" + str(int(from_t))
        url += "&to=" + str(int(to_t))
        rj = None
        try:
            resp = self.sess.get(
                url,
                auth=(
                    GetDeviceData.bt_user,
                    GetDeviceData.bt_pass))
            rj = resp.json()
        except Exception as e:
            send_error(str(e))
            return 0

        load_count = 0
        if rj is not None and len(rj) > 0:
            for esdoc in rj:
                esdoc["ts"] = GetDeviceData.convert_ts(esdoc["ts"])
                esdoc["name"] = dev[0].strip()
                esdoc["vendor"] = dev[2]
                esdoc["deviceType"] = dev[3]
                esdoc["deviceId"] = dev[4]
                esdoc["doctor"] = dev[1].strip()
                if esdoc["doctor"] is None or len(esdoc["doctor"]) < 1:
                    esdoc["doctor"] = "unknown"

                if dev[3] == "scale":
                    if "values" in esdoc and "weight" in esdoc["values"]:
                        esdoc["weight_lbs"] = round(
                            esdoc["values"]["weight"] * GetDeviceData.LBS_PER_G, 1)

                elif dev[3] == "bp":
                    if "values" in esdoc and "systolic" in esdoc["values"]:
                        esdoc["systolic_mmhg"] = round(
                            esdoc["values"]["systolic"] * GetDeviceData.MMHG_PER_PA, 0)
                        esdoc["diastolic_mmhg"] = round(
                            esdoc["values"]["diastolic"] * GetDeviceData.MMHG_PER_PA, 0)
                        esdoc["pulse_bpm"] = esdoc["values"]["pulse"]

                if "values" in esdoc:
                    id = esdoc["deviceId"] + esdoc["ts"]
                    self.post_doc(esdoc, id)
                    load_count += 1

        return load_count


def main():
    hdt = dt.datetime.utcnow().replace(
        minute=0, second=0, microsecond=0, tzinfo=dt.timezone.utc)
    to_t = hdt.timestamp() * 1000.0
    hdt2 = hdt + dt.timedelta(hours=-1)
    from_t = hdt2.timestamp() * 1000.0

    if argv[1] == "catchup":
        TIME_PARSE = "%Y-%m-%d_%H:%M %Z"
        from_t = dt.datetime.strptime(
            argv[2] + " UTC",
            TIME_PARSE).timestamp() * 1000.0
        to_t = dt.datetime.strptime(
            argv[3] + " UTC",
            TIME_PARSE).timestamp() * 1000.0

    GetDeviceData().fetch_all(from_t, to_t)


if __name__ == "__main__":
    main()
