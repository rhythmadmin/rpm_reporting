"""The RPM Dashboard web console.

    When this file is ran, Sanic instantiates a new instance of the Dashboard in port 8080.
    The Dashboard is a web console that allows you to interact with the RPM database.
    API calls are made to the RPM database and the results are returned to the Dashboard when using Swagger.

    To access Swagger, go to http://localhost:8080/swagger/

    Generation of Excel spreadsheets and patient PDF records is also possible.
    Modification of a patient's record is also possible.
"""

import datetime as dt
import schedule

from os import getenv
from sanic import Sanic
from sanic import response
from sanic.response import file
from sanic.log import logger

from sanic_openapi import doc, swagger_blueprint

from sheet_builder import create_workbook, patient_list_spreadsheet
from db import add_patient, es_query, get_patients, pdf_query, latest_sync, remove_patient, update_fetch_time, get_latest_fetch, schedule_data_pull, \
    run_jobs, update_doctor, june, july, august, september, october, november, december, january, february
from pdf_builder import generate_pdf
from integration import GetDeviceData
from errors import get_errors, send_error

import os
import openpyxl
import pandas


def init():
    ES = getenv('ES_PASS')
    BT = getenv('BT_PASS')
    IG = getenv('IG_KEY')
    if not ES or not BT or not IG:
        print('One of the env vars needed are not set')
        print('Please read the README.md file')
        exit(1)


app = Sanic("RPM Spreadsheet Download")

app.blueprint(swagger_blueprint)

# Set timeout for 3 minutes to allow sheet processing time.
app.config.RESPONSE_TIMEOUT = 180

app.static('/static', './html/static')


@app.route('/health')
@doc.summary("Test endpoint that returns a response of \"ok\".")
@doc.tag("test")
@doc.produces(doc.String(description="The \"ok\" response."),
              content_type='text/plain')
async def health(request):
    return response.text("ok")


@app.route('/sync-data')
@doc.summary("Syncs data in ElasticSearch and redirects to the main index.")
@doc.tag("es operation")
@doc.produces(doc.File(description="The HTML of the main index page."),
              content_type='text/html')
async def fetch_data(request):
    hdt = dt.datetime.utcnow().replace(
        minute=0, second=0, microsecond=0, tzinfo=dt.timezone.utc)
    to_t = hdt.timestamp() * 1000.0
    hdt2 = hdt + dt.timedelta(hours=-1)
    from_t = hdt2.timestamp() * 1000.0

    GetDeviceData().fetch_all(from_t, to_t)
    update_fetch_time()
    return response.redirect('/')


@app.route('/latest-record')
@doc.summary("Gets the timestamp of the most recent patient record recorded in ElasticSearch.")
@doc.tag("patient query")
@doc.produces(doc.String(description="The timestamp in an easy-to-read format."),
              content_type='text/plain')
async def latest_record(request):
    return response.text(latest_sync())


@app.route('/latest-fetch')
@doc.summary("Gets the timestamp of the most recent data pull performed in ElasticSearch.")
@doc.tag("es operation")
@doc.produces(doc.String(description="The lastest data pull timestamp."),
              content_type='text/plain')
async def latest_fetch(request):
    return response.text(get_latest_fetch())


@app.route('/get-weight-sheet-data', methods=['POST'])
@doc.summary("Gets the data for the weight tab in the spreadsheet and returns it as JSON.")
@doc.tag('spreadsheet data')
@doc.consumes(doc.String(description="The month to fetch weight data for.", name="Month"),
              required=True, location="formData", content_type="multipart/form-data")
@doc.consumes(doc.String(description="The year to fetch weight data for.", name="Year"),
              required=True, location="formData", content_type="multipart/form-data")
@doc.produces(doc.JsonBody(description="The weight sheet data returned as JSON."))
async def get_weight_sheet_data(request):

    file_month = request.form.get('Month')
    file_year = request.form.get('Year')

    filename = './RPM Summary {} {}.xlsx'.format(file_month, file_year)

    if os.path.isfile(filename):
        openpyxl.load_workbook(filename)
        excel_dataframe = pandas.read_excel(filename, sheet_name="Weight")
        return response.json(excel_dataframe.to_json(orient='records'))
    else:
        return response.json(
            {"error": "The file does not exist. Please create the spreadsheet and try again."})


@app.route('/get-systolic-sheet-data', methods=['POST'])
@doc.summary("Gets the data for the systolic tab in the spreadsheet and returns it as JSON.")
@doc.tag("spreadsheet data")
@doc.consumes(doc.String(description="The month to fetch systolic data for.", name="Month"),
              required=True, location="formData", content_type="multipart/form-data")
@doc.consumes(doc.String(description="The year to fetch systolic data for.", name="Year"),
              required=True, location="formData", content_type="multipart/form-data")
@doc.produces(doc.JsonBody(description="The systolic sheet data returned as JSON."))
async def get_systolic_sheet_data(request):

    file_month = request.form.get('Month')
    file_year = request.form.get('Year')

    filename = './RPM Summary {} {}.xlsx'.format(file_month, file_year)

    if os.path.isfile(filename):
        openpyxl.load_workbook(filename)
        excel_dataframe = pandas.read_excel(filename, sheet_name="Systolic")
        return response.json(excel_dataframe.to_json(orient="records"))
    else:
        return response.json(
            {"error": "The file does not exist. Please create the spreadsheet and try again."})


@app.route('/get-diastolic-sheet-data', methods=['POST'])
@doc.summary("Gets the data for the diastolic tab in the spreadsheet and returns it as JSON.")
@doc.tag("spreadsheet data")
@doc.consumes(
    doc.String(
        description="The month to fetch diastolic data for.",
        name="Month"),
    required=True,
    location="formData",
    content_type="multipart/form-data")
@doc.consumes(doc.String(description="The year to fetch diastolic data for.", name="Year"),
              required=True, location="formData", content_type="multipart/form-data")
@doc.produces(doc.JsonBody(description="The diastolic sheet data returned as JSON."))
async def get_diastolic_sheet_data(request):

    file_month = request.form.get('Month')
    file_year = request.form.get('Year')

    filename = './RPM Summary {} {}.xlsx'.format(file_month, file_year)

    if os.path.isfile(filename):
        openpyxl.load_workbook(filename)
        excel_dataframe = pandas.read_excel(filename, sheet_name="Diastolic")
        return response.json(excel_dataframe.to_json(orient="records"))
    else:
        return response.json(
            {"error": "The file does not exist. Please create the spreadsheet and try again."})


@app.route('/get-pulse-sheet-data', methods=['POST'])
@doc.summary("Gets the data for the pulse tab in the spreadsheet and returns it as JSON.")
@doc.tag("spreadsheet data")
@doc.consumes(doc.String(description="The month to fetch pulse data for.", name="Month"),
              required=True, location="formData", content_type="multipart/form-data")
@doc.consumes(doc.String(description="The year to fetch pulse data for.", name="Year"),
              required=True, location="formData", content_type="multipart/form-data")
@doc.produces(doc.JsonBody(description="The pulse sheet data returned as JSON."))
async def get_pulse_sheet_data(request):

    file_month = request.form.get('Month')
    file_year = request.form.get('Year')

    filename = './RPM Summary {} {}.xlsx'.format(file_month, file_year)

    if os.path.isfile(filename):
        openpyxl.load_workbook(filename)
        excel_dataframe = pandas.read_excel(filename, sheet_name="Pulse")
        return response.json(excel_dataframe.to_json(orient="records"))
    else:
        return response.json(
            {"error": "The file does not exist. Please create the spreadsheet and try again."})


@app.route('/get-glucose-sheet-data', methods=['POST'])
@doc.summary("Gets the data for the glucose tab in the spreadsheet and returns it as JSON.")
@doc.tag("spreadsheet data")
@doc.consumes(doc.String(description="The month to fetch glucose data for.", name="Month"),
              required=True, location="formData", content_type="multipart/form-data")
@doc.consumes(doc.String(description="The year to fetch glucose data for.", name="Year"),
              required=True, location="formData", content_type="multipart/form-data")
@doc.produces(doc.JsonBody(description="The glucose sheet data returned as JSON."))
async def get_glucose_sheet_data(request):

    file_month = request.form.get('Month')
    file_year = request.form.get('Year')

    filename = './RPM Summary {} {}.xlsx'.format(file_month, file_year)

    if os.path.isfile(filename):
        openpyxl.load_workbook(filename)
        excel_dataframe = pandas.read_excel(filename, sheet_name="Glucose")
        return response.json(excel_dataframe.to_json(orient="records"))
    else:
        return response.json(
            {"error": "The file does not exist. Please create the spreadsheet and try again."})


@app.route('/get-compliance-sheet-data', methods=['POST'])
@doc.summary("Gets the data for the compliance tab in the spreadsheet and returns it as JSON.")
@doc.tag("spreadsheet data")
@doc.consumes(
    doc.String(
        description="The month to fetch compliance data for.",
        name="Month"),
    required=True,
    location="formData",
    content_type="multipart/form-data")
@doc.consumes(doc.String(description="The year to fetch compliance data for.", name="Year"),
              required=True, location="formData", content_type="multipart/form-data")
@doc.produces(doc.JsonBody(description="The compliance sheet data returned as JSON."))
async def get_compliance_sheet_data(request):

    file_month = request.form.get('Month')
    file_year = request.form.get('Year')

    filename = './RPM Summary {} {}.xlsx'.format(file_month, file_year)

    if os.path.isfile(filename):
        openpyxl.load_workbook(filename)
        excel_dataframe = pandas.read_excel(filename, sheet_name="Compliance")
        return response.json(excel_dataframe.to_json(orient="records"))
    else:
        return response.json(
            {"error": "The file does not exist. Please create the spreadsheet and try again."})


@app.route('/get-daily-report-sheet-data', methods=['POST'])
@doc.summary("Gets the data for the daily report tab in the spreadsheet and returns it as JSON.")
@doc.tag("spreadsheet data")
@doc.consumes(
    doc.String(
        description="The month to fetch daily report data for.",
        name="Month"),
    required=True,
    location="formData",
    content_type="multipart/form-data")
@doc.consumes(
    doc.String(
        description="The year to fetch daily report data for.",
        name="Year"),
    required=True,
    location="formData",
    content_type="multipart/form-data")
@doc.produces(doc.JsonBody(description="The daily report sheet data returned as JSON."))
async def get_daily_report_sheet_data(request):

    file_month = request.form.get('Month')
    file_year = request.form.get('Year')

    filename = './RPM Summary {} {}.xlsx'.format(file_month, file_year)

    if os.path.isfile(filename):
        openpyxl.load_workbook(filename)
        excel_dataframe = pandas.read_excel(
            filename, sheet_name="Daily Report")
        return response.json(excel_dataframe.to_json(orient="records"))
    else:
        return response.json(
            {"error": "The file does not exist. Please create the spreadsheet and try again."})


@app.route('/doctor-patient-list')
@doc.summary("Gets the most recent patient list in ElasticSearch.")
@doc.tag("patient query")
@doc.produces(
    doc.JsonBody(
        description="The list of patients separated by doctor name in JSON format."),
    content_type='application/json')
async def doctor_patient(request):
    return response.json(pdf_query())


@app.route('/patient-table-data')
@doc.summary("Gets a list of patient names, doctor names, phone numbers, and device IDs returned in JSON format for data ingestion.")
@doc.tag("patient query")
@doc.produces(doc.JsonBody(description="The list of patient data returned as JSON."),
              content_type='application/json')
async def patient_table_data(request):
    return response.json(get_patients()['patients'])


@app.route('/doctor-table-data')
@doc.summary("Gets a list of doctor-specified ranges for patient alerts and returns them in JSON format.")
@doc.tag("patient query")
@doc.produces(
    doc.JsonBody(
        description="The list of alert range data returned as JSON."),
    content_type='application/json')
async def doctor_table_data(request):
    return response.json(get_patients()['doctor_alerts'])


@app.route('/errors-table-data')
@doc.summary("Gets a list of json errors that occurred while trying to retrieve patient data.")
@doc.tag("error handling")
@doc.produces(doc.JsonBody(description="The list of errors in JSON format."),
              content_type='application/json')
async def errors_table_data(request):
    return response.json(get_errors())


@app.route('/patient-list-spreadsheet')
@doc.summary("Creates a spreadsheet based on the patients that are currently in the ES cluster.")
@doc.tag("patient query")
@doc.produces(doc.File(description="The spreadsheet containing the master list of patients."),
              content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
async def patient_list(request):
    patient_list_spreadsheet(get_patients()['patients'])
    return await response.file("./RPM Patient List.xlsx",
                               mime_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                               filename='RPM Patient List.xlsx')


@app.route('/create-patient', methods=['POST'])
@doc.summary("Creates a new patient using the supplied information in the HTML form.")
@doc.tag("es operation")
@doc.consumes(doc.String(description='The field for the patient name.',
                         name="Patient Name"),
              required=True,
              location='formData',
              content_type='multipart/form-data')
@doc.consumes(
    doc.String(
        description='The field for the patient\'s doctor\'s name.',
        name="Doctor Name"),
    required=True,
    location='formData',
    content_type='multipart/form-data')
@doc.consumes(doc.String(description='The field for the patient\'s phone number.',
              name="Phone Number"), location='formData', content_type='multipart/form-data')
@doc.consumes(
    doc.String(
        description='The field for the patient\'s BodyTrace Device ID.',
        name="BodyTrace Device ID"),
    location='formData',
    content_type='multipart/form-data')
@doc.consumes(
    doc.String(
        description='The field for the patient\'s BodyTrace Scale Device ID.',
        name="BodyTrace Scale DeviceID"),
    location='formData',
    content_type='multipart/form-data')
@doc.consumes(
    doc.String(
        description='The field for the patient\'s Smart Meter BP Device ID.',
        name="Smart Meter BP DeviceID"),
    location='formData',
    content_type='multipart/form-data')
@doc.consumes(
    doc.String(
        description='The field for the patient\'s Smart Meter Glucometer Device ID.',
        name="Smart Meter Glucometer DeviceID"),
    location='formData',
    content_type='multipart/form-data')
@doc.consumes(
    doc.String(
        description='The field for the patient\'s SmartMeter Scale Device ID.',
        name="SmartMeter Scale DeviceID"),
    location='formData',
    content_type='multipart/form-data')
@doc.produces(doc.File(description="The HTML for the main index page."),
              content_type="text/html")
async def create_patient(request):
    if not request.form.get(
            'Patient Name') or not request.form.get('Doctor Name'):
        return await response.json({'error': 'missing patient or doctor in POST body'})

    patient = request.form.get('Patient Name').strip()
    doctor = request.form.get('Doctor Name').strip()
    phone = request.form.get('Phone Number')
    bt_bp = request.form.get('BodyTrace BP DeviceID')
    bt_scale = request.form.get('BodyTrace Scale DeviceID')
    sm_bp = request.form.get('SM BP DeviceID')
    sm_glucometer = request.form.get('SM Glucometer DeviceID')
    sm_scale = request.form.get('SM Scale DeviceID')

    try:
        if bt_bp:
            bt_bp = str(bt_bp).rstrip('.').strip()
        if bt_scale:
            bt_scale = str(bt_scale).rstrip('.').strip()
        if sm_bp:
            sm_bp = str(sm_bp).rstrip('.').strip()
        if sm_glucometer:
            sm_glucometer = str(sm_glucometer).rstrip('.').strip()
        if sm_scale:
            sm_scale = str(sm_scale).rstrip('.').strip()

    except BaseException as error:
        err = "error converting device IDs: " + str(error)
        logger.error(err)
        send_error(err)
        return await response.json({'error': 'error converting device IDs'})

    if patient and doctor:
        if bt_bp or bt_scale or sm_bp or sm_glucometer or sm_scale:

            data = {
                'Patient Name': patient,
                'Doctor Name': doctor,
                'Phone Number': phone,
                'BodyTrace BP DeviceID': bt_bp,
                'BodyTrace Scale DeviceID': bt_scale,
                'SM BP DeviceID': sm_bp,
                'SM Glucometer DeviceID': sm_glucometer,
                'SM Scale DeviceID': sm_scale}

            resp = add_patient(data)

            if resp.status_code > 300:
                err = "error adding patient, response: " + str(resp)
                logger.error(err)
                send_error(err)
                return await response.json({'error': 'error creating patient'})

    return response.redirect('/patients.html')


@app.route('/delete-patient')
@doc.summary("Deletes a patient using their name and doctor name.")
@doc.tag("es operation")
@doc.consumes(doc.String(description="The name of the patient to delete.",
                         name="patient"),
              required=True,
              location="formData",
              content_type="multipart/form-data")
@doc.consumes(doc.String(description="The name od the patient's doctor.",
                         name="doctor"),
              required=True,
              location="formData",
              content_type="multipart/form-data")
@doc.produces(doc.File(description="The HTML for the main index page."),
              content_type='text/html')
async def delete_patient(request):
    if not request.args.get("patient") or not request.args.get("doctor"):
        return await response.json({'error': 'doctor or patient missing from POST body'})

    patient = request.args.get("patient").strip()
    doctor = request.args.get("doctor").strip()

    resp = remove_patient(patient, doctor)
    if resp.status_code > 300:
        err = "error removing patient, response: " + str(resp)
        logger.error(err)
        send_error(err)
        return await response.json({'error': 'error removing patient'})

    return response.redirect('/patients.html')


@app.route('/update-doctor', methods=['POST'])
@doc.summary("Updates doctor ranges in accordance to specified values in the modal form.")
@doc.tag("es operation")
@doc.consumes(doc.String(description="The name of the doctor.",
                         name="Doctor Name"),
              required=True,
              location="body",
              content_type='text/plain')
@doc.consumes(
    doc.String(
        description="The 1-day weight delta for the specified doctor.",
        name="Weight1"),
    required=True,
    location="formData",
    content_type="multipart/form-data")
@doc.consumes(
    doc.String(
        description="The 3-day weight delta for the specified doctor.",
        name="Weight7"),
    required=True,
    location="formData",
    content_type='multipart/form-data')
@doc.consumes(
    doc.String(
        description="The maximum pulse value for the specified doctor.",
        name="PulseMax"),
    required=True,
    location="formData",
    content_type="multipart/form-data")
@doc.consumes(
    doc.String(
        description="The minimum pulse value for the specified doctor.",
        name="PulseMin"),
    required=True,
    location="formData",
    content_type="multipart/form-data")
@doc.consumes(
    doc.String(
        description="The maximum systolic value for the specified doctor.",
        name="SysMax"),
    required=True,
    location="formData",
    content_type="multipart/form-data")
@doc.consumes(
    doc.String(
        description="The minimum systolic value for the specified doctor.",
        name="SysMin"),
    required=True,
    location="formData",
    content_type="multipart/form-data")
@doc.consumes(
    doc.String(
        description="The maximum diastolic value for the specified doctor.",
        name="DiaMax"),
    required=True,
    location="formData",
    content_type="multipart/form-data")
@doc.consumes(
    doc.String(
        description="The minimum diastolic value for the specified doctor.",
        name="DiaMin"),
    required=True,
    location="formData",
    content_type="multipart/form-data")
@doc.consumes(
    doc.String(
        description="The maximum glucose value for the specified doctor.",
        name="GluMax"),
    required=True,
    location="formData",
    content_type="multipart/form-data")
@doc.consumes(
    doc.String(
        description="The minimum glucose value for the specified doctor.",
        name="GluMin"),
    required=True,
    location="formData",
    content_type="multipart/form-data")
@doc.produces(doc.File(description="The HTML for the main index page."),
              content_type='text/html')
async def update_doctor_data(request):

    doctor = request.form.get("Doctor Name").strip()
    weight_1 = request.form.get("Weight1").strip()
    weight_7 = request.form.get("Weight7").strip()
    pulse_max = request.form.get("PulseMax").strip()
    pulse_min = request.form.get("PulseMin").strip()
    systolic_max = request.form.get("SysMax").strip()
    systolic_min = request.form.get("SysMin").strip()
    diastolic_max = request.form.get("DiaMax").strip()
    diastolic_min = request.form.get("DiaMin").strip()
    glucose_max = request.form.get("GluMax").strip()
    glucose_min = request.form.get("GluMin").strip()

    data = {
        "Doctor Name": doctor,
        "pulse_gt": pulse_max,
        "pulse_lt": pulse_min,
        "systolic_gt": systolic_max,
        "systolic_lt": systolic_min,
        "diastolic_gt": diastolic_max,
        "diastolic_lt": diastolic_min,
        "glucose_gt": glucose_max,
        "glucose_lt": glucose_min,
        "weight_1_day_gt": weight_1,
        "weight_7_day_gt": weight_7
    }

    resp = update_doctor(data)

    if resp.status_code > 300:
        err = "error updating doctor, response: " + str(resp)
        logger.error(err)
        send_error(err)
        return await response.json({'error': 'error updating doctor'})

    return response.redirect('/')


@app.route('/patients.html')
@doc.summary("The route for the patients page in the main dashboard.")
@doc.tag("route")
@doc.produces(doc.File(description="The HTML for the patient page."),
              content_type='text/html')
async def patients(request):
    return await file("./html/patients.html", mime_type="text/html")


@app.route('/')
@doc.summary("The route for the main dashboard's index page.")
@doc.tag("route")
@doc.produces(doc.File(description="The HTML for the main dashboard's index page."),
              content_type='text/html')
async def index(request):
    return await file("./html/index.html", mime_type="text/html")


@app.route('/errors.html')
@doc.summary("The route for the errors page in the main dashboard.")
@doc.tag("route")
@doc.produces(doc.File(description='The HTML for the errors page.'),
              content_type='text/html')
async def errors(request):
    return await file("./html/errors.html", mime_type="text/html")


@app.route('/create-pdf', methods=['POST'])
@doc.summary("Creates a PDF of a patient's vitals history for the selected month.")
@doc.tag("patient query")
@doc.consumes(doc.String(description="The name of the patient.",
                         name="patient"),
              required=True,
              location="formData",
              content_type="multipart/form-data")
@doc.consumes(doc.String(description="The name of the patient's doctor.",
                         name="doctor"),
              required=True,
              location="formData",
              content_type="multipart/form-data")
@doc.consumes(doc.String(description="The month to generate the PDF for.", name="month"),
              required=True, location="formData", content_type="multipart/form-data")
@doc.consumes(
    doc.String(
        description="Any notes to be added to the end of the PDF document.",
        name="notes"),
    location="formData",
    content_type="multipart/form-data")
@doc.produces(doc.File(description="The generated PDF of patient vitals."),
              content_type='application/pdf')
async def create_pdf(request):
    report_name = ''

    # Generate PDF Report
    doctor = request.form.get("doctor").strip()
    patient = request.form.get("patient").strip()
    month = request.form.get("month")
    notes = request.form.get("notes")

    if not doctor or not patient or not month:
        return await response.json({'error': 'missing doctor, patient or month in POST body'})

    if month == 'June':
        report_name = generate_pdf(
            es_query(june), doctor, patient, month, notes)

    elif month == 'July':
        report_name = generate_pdf(
            es_query(july), doctor, patient, month, notes)

    elif month == 'August':
        report_name = generate_pdf(
            es_query(august), doctor, patient, month, notes)

    elif month == 'September':
        report_name = generate_pdf(
            es_query(september), doctor, patient, month, notes)

    elif month == 'October':
        report_name = generate_pdf(
            es_query(october), doctor, patient, month, notes)

    elif month == 'November':
        report_name = generate_pdf(
            es_query(november), doctor, patient, month, notes)

    elif month == 'December':
        report_name = generate_pdf(
            es_query(december), doctor, patient, month, notes)

    elif month == 'January':
        report_name = generate_pdf(
            es_query(january), doctor, patient, month, notes)

    elif month == 'February':
        report_name = generate_pdf(
            es_query(february), doctor, patient, month, notes)

    return await response.file("./" + report_name, mime_type="application/pdf", filename=report_name)


@app.route('/create-monthly-spreadsheet')
@doc.summary('Generates a spreadsheet based on the selected month from the main index page.')
@doc.tag("patient query")
@doc.consumes(
    doc.String(
        description="The month that the spreadsheet will be generated for.",
        name="month"),
    required=True,
    location="body",
    content_type="text/plain")
@doc.produces(doc.File(description="The generated spreadsheet."),
              content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
async def create_monthly_spreadsheet(request):
    report_name = ''

    month = request.args.get("month")

    if not month:
        return await response.json({'error': 'month not provided as request arg'})

    # Generate Spreadsheet Report
    if month == "june":
        report_name = 'RPM Summary June 2021.xlsx'
        create_workbook(es_query(june), report_name)

    elif month == "july":
        report_name = 'RPM Summary July 2021.xlsx'
        create_workbook(es_query(july), report_name)

    elif month == "august":
        report_name = 'RPM Summary August 2021.xlsx'
        create_workbook(es_query(august), report_name)

    elif month == "september":
        report_name = 'RPM Summary September 2021.xlsx'
        create_workbook(es_query(september), report_name)

    elif month == "october":
        report_name = 'RPM Summary October 2021.xlsx'
        create_workbook(es_query(october), report_name)

    elif month == "november":
        report_name = 'RPM Summary November 2021.xlsx'
        create_workbook(es_query(november), report_name)

    elif month == "december":
        report_name = 'RPM Summary December 2021.xlsx'
        create_workbook(es_query(december), report_name)

    elif month == "january":
        report_name = 'RPM Summary January 2021.xlsx'
        create_workbook(es_query(january), report_name)

    elif month == "february":
        report_name = 'RPM Summary February 2021.xlsx'
        create_workbook(es_query(february), report_name)

    return await response.file("./" + report_name,
                               mime_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                               filename=report_name)


if __name__ == '__main__':
    init()
    schedule.every(15).minutes.do(schedule_data_pull)
    schedule.every(2).hours.do(schedule_data_pull, resync=True)
    run_jobs()
    app.run(host='127.0.0.1', port=8080, debug=False)
